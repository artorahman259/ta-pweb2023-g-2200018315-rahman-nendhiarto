<!DOCTYPE html>
<html>
<head>
    <title>Program Penilaian</title>
    <style>
        body {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100vh;
            font-family: Arial, sans-serif;
        }
            h1 {
        color: olivedrab;
    }

    form {
        margin-bottom: 20px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    label {
        font-weight: bold;
        margin-right: 10px;
    }

    input[type="text"] {
        padding: 5px;
    }

    button[type="submit"] {
        padding: 5px 10px;
        background-color: black;
        color: white;
        border: none;
        cursor: pointer;
    }

    .hasil {
        margin-top: 20px;
        padding: 10px;
        border-radius: 5px;
        text-align: center;
        font-size: 20px;
        font-weight: bold;
    }

    .lulus {
        background-color: green;
        color: white;
    }

    .gagal {
        background-color: red;
        color: white;
    }
</style>
</head>
<body>
    <h1>Program Penilaian</h1>
    <form method="post" action="">
        <label for="nilai">Masukkan Nilai:</label>
        <input type="text" name="nilai" id="nilai" required>
        <button type="submit">Submit</button>
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nilai = $_POST["nilai"];
        $nilaiHuruf = konversiNilaiHuruf($nilai);
        $sks = konversiNilaiKeSKS($nilaiHuruf);
        echo "<div class='hasil " . ($nilaiHuruf == 'E' ? 'gagal' : 'lulus') . "'>Nilai Anda $nilai, Anda $nilaiHuruf. Anda dapat mengambil $sks SKS</div>";
    }
    function konversiNilaiHuruf($nilaiAngka) {
    if ($nilaiAngka >= 80) {
        return 'A';
    } elseif ($nilaiAngka >= 70) {
        return 'B';
    } elseif ($nilaiAngka >= 60) {
        return 'C';
    } elseif ($nilaiAngka >= 50) {
        return 'D';
    } else {
        return 'E';
    }
}

function konversiNilaiKeSKS($nilaiHuruf) {
    if ($nilaiHuruf == 'A') {
        return 24;
    } elseif ($nilaiHuruf == 'B') {
        return 22;
    } elseif ($nilaiHuruf == 'C') {
        return 20;
    } elseif ($nilaiHuruf == 'D') {
        return 18;
    } else {
        return 15;
    }
}
?>
</body>
</html>
