<!DOCTYPE html>
<html>
<head>
    <title>Program Nilai</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            text-align: center;
            margin-top: 200px;
        }
        
        h1 {
            color: black;
        }
        
        p {
            color: slategray;
            font-size: 18px;
        }
        
        .lulus {
            color: green;
            font-weight: bold;
        }
        
        .tidak-lulus {
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <h1>Program Nilai</h1>
    <form method="post" action="">
        Masukkan nilai Anda: <input type="number" name="nilai" min="0" max="100" required>
        <input type="submit" value="Submit">
    </form>
    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $nilai = $_POST['nilai'];
        if ($nilai >= 60) {
            echo "<h2>Selamat!</h2>";
            echo "<p class='lulus'>Nilai Anda $nilai, Anda lulus</p>";
        } else {
            echo "<h2>Maaf!</h2>";
            echo "<p class='tidak-lulus'>Nilai Anda $nilai, Anda tidak lulus</p>";
        }
    }
    ?>
</body>
</html>
